<html>
  <head>
  <title>Hangman game by Tomek Czurak</title>
  <link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css'>
  <script>
  document.onload = function() {
  document.getElementById('theInput').focus();
  };
  </script>
  <style>
    #controls {
      float:left;
      max-width:450px;
    }
    #hangman {
      float:left;
      max-width:500px;
      margin-top:30px;
    }
    #hangman img{
    margin-top:25px;
    float:left;
    }
    .letter {
      float:left;
      margin:0px 5px 0px 5px;
      width:30px;
      font-size:24px;
      border-bottom:1px solid #fff;
      text-align:center;
    }
    .dotted {
    margin:40px 0px 70px 0px;;
    border: 1px dashed #ddd;
    padding:10px;
    text-align:center;
    float:left;
    }
    .caps {
   text-transform:uppercase;
   font-weight:bold;
    }
    body {
      font-family: 'Gloria Hallelujah', cursive;
      color:#fff;
      padding:15px 45px;
      background-color:#000;
      background-image: url('img/bg.jpg');
    }
    a {
    color:#fff;
    text-decoration:underline;
    }
    a:hover {
    font-weight:bold;
    }
    h1 {
    margin-bottom:45px;
    }
    td {
      vertical-align:sub;
    }
  </style>

  </head>
  <body>
      <div id="controls">
     <?php if ($game->hangman->saved) { ?>
        <h1>You have won! CONGRATULATIONS!</h1> <?php }
        elseif ($game->hangman->dead) { ?>
        <h1>You LOSE! HAHA!</h1>
      <?php } else { ?>
           <p><h1>Save that man from hanging!</h1></p>
           <p>
           <form id="theForm" action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" type="text/plain" method="GET">
           <table>
           <tr>
           <td>Guess the letter:</td>
           <td width="15"></td>
           <td><input id="theInput"  type="text" name="letter" size="10" onkeyup="document.getElementById('theForm').submit();" autofocus>
           </td>
           </tr>
           </table>
           </form>
           </p>
    <?php } ?>
      <p>Wrong guesses: <span class="caps"><?php  foreach ($game->wrong_letters as $wrong_letter) { 	echo $wrong_letter . " ";} ?></span></p>
      <?php if (empty($game->hint)) { ?>
          <p><a href="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>?hint=1">Get a hint</a> <small>(will cost you an arm or a leg)</small></a></p>
      <?php } else {
      echo "<p>Your hint: <em>" . $game->hint . "</em></p>";
      } ?>
    </div>
    <div id="hangman">
    <?php
    if ($game->hangman->dead) {
    	    foreach ($game->lettered_word as $key=>$letter) {
    	    	    echo "<div class=\"letter\">" . $letter . "</div>";
    	    }
    } else {
    	    foreach ($game->guessed_letters as $key=>$letter) {
    	    	    if (empty($letter)) { $letter = "&nbsp;"; }
    	    	    echo "<div class=\"letter\">" . $letter . "</div>";
    	    }
    }
      ?>
      <?php if ($i = $game->hangman->lost_parts) { echo "<img src=\"img/" . $i . ".png\">"; } else { echo "<img src=\"img/0.png\">"; }?>
    </div>
    <div style="clear:both;"></div>
    <div class="dotted">
        <p><a href="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>?newgame=1">New game</a> | <a href="http://www.iamtomek.com/my-blog/hangman-php-project/">Get source code (PHP)</a></p>
      </div>

    
  </body>
</html>