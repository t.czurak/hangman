<?php

class Words {
	private $entries;
	public $word;
	public $definition;
	
	 function __construct() {
	 	 //randomly get the word/definition from the text file to use in a game
		$content = file_get_contents("words.txt");
		$this->entries = explode("\n", $content);
		shuffle($this->entries);
		$entry = explode("|", $this->entries[0]);
		$this->word = $entry[0];
		$this->definition = $entry[1];
	}
	
	function GetWord() {
		return $this->word;
	}
	
	function GetDefinition() {
		return $this->definition;
	}
}

class Hangman {
	public $lost_parts = 0;
	public $max_parts = 8;
	public $saved;
	public $dead;
	
	function StepDeathRow() {
		//wrong letter guessed
		++$this->lost_parts;
		if ($this->lost_parts == $this->max_parts) { 
			$this->dead = TRUE; 
		}
	}
	
	function SetSaved() {
		//you win
		$this->saved = TRUE;
	}
}

class GameLogic {
	public $lettered_word;
	public $guessed_letters;
	public $wrong_letters;
	public $hint;
	public $words;  //object of Words class
	public $hangman; //object of Hangman class
	
	function __construct($words, $hangman) {
		$this->words = $words;
		$this->hangman = $hangman;
		$this-> wrong_letters = array();
		
		//split word into letters
		$this->lettered_word = str_split($this->words->word);
		$i = count($this->lettered_word);
		
		//prepare an empty array 
		$this-> guessed_letters = array();
		while ($i >0) {
			array_push($this-> guessed_letters, null);
			--$i;
		}
	}
	
	function compare($user_letter) {
		$keys = array_keys($this->lettered_word, $user_letter);
		if ($keys) {
			foreach ($keys as $key) {
				$this->guessed_letters[$key] = $user_letter;	
			}
			//if a user guessed all letters, she wins
			if ($this->lettered_word == $this->guessed_letters) {
				$this->hangman->SetSaved();
			}
		} else {
			$this->hangman->StepDeathRow();
			array_push($this->wrong_letters,$user_letter);
		}
	}
	
	function GetHint() {
		//get the definition of a wors and remove one part of a hangman
		if (empty($this->hint)) { 
			$this->hint = $this->words->definition;
			$this->hangman->StepDeathRow();
		}
	}
}

class Display {
	static function template($game) {
		include("template.php");
	}
}

/* *****************************************
*Session class serializes/unserializes objects
*and stores them in a session
******************************************* */
class Session {
	function __construct() {
		session_start();
	}
	
	function StoreObject($object) {
		$_SESSION['game'] = serialize($object);
	}
	
	function GetObject() {
		$object = unserialize($_SESSION['game']); 
		if (!$object) {return NULL; } else { return $object; }
	}
	
	function WipeSession() {
		session_destroy();
	}
}
?>