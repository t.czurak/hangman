<?php
require_once("classes.php");

if ($_GET['newgame']) {
	$session  = new Session();
	$game =  $session->WipeSession();
}

if (!$_GET['letter']) {
	$session  = new Session();
	if ($session->GetObject() == NULL) {
		$hangman  = new hangman();
		$words = new words();
		$game = new GameLogic($words, $hangman);
		$session->StoreObject($game);
	} else {
		$game = $session->GetObject();
	}
}

if ($_GET['letter']) {
	$session  = new Session();
	$game =  $session->GetObject();
	$game->compare($_GET['letter']);
	$session->StoreObject($game);
}

if ($_GET['hint']) {
	$session  = new Session();
	$game =  $session->GetObject();
	$game->GetHint();
	$session->StoreObject($game);
}

Display::template($game);
?>